package com.braz.dao;

import java.util.List;

import com.braz.entities.Passageiro;

public interface PassageiroDao {
	public void salvarPassageiro(Passageiro p);
	public List<Passageiro> passageiros();
}

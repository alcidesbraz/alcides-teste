package com.braz.dao;

import java.util.List;

import com.braz.entities.Corrida;

public interface CorridaDao {
	public void salvarCorrida(Corrida c);
	public List<Corrida> corridas();
}

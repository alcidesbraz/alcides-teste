package com.braz.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.braz.entities.Passageiro;

public class PassageiroDaoImpl implements PassageiroDao {

	private EntityManager em;

	public PassageiroDaoImpl() {
		em = new ConnectionFactory().getConnection();
	}

	@Override
	public void salvarPassageiro(Passageiro p) {
		try {
			em.getTransaction().begin();
			em.persist(p);
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
	}

	@Override
	public List<Passageiro> passageiros() {
		Query query = em.createQuery("select c from Passageiro c");
		List passageiro = new ArrayList<Passageiro>();
		passageiro = query.getResultList();
		return passageiro;
	}
}
